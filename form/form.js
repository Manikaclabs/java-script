$("document").ready(function ()
{   $(".error").hide(); //to hide error statements
    //to validate date
    var date;
    date =new Date();
    var day = date.getDate();
    var month = date.getMonth();
    month=month+1;
    if(month<10)
    {
        month="0"+month;
    }
    var year =date.getFullYear();
    var today=(year+"-"+month+"-"+day);console.log(today);
    $("#start-date").attr("max",today);
    $("#submit").click(function ()
    {   function showError()
        {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            setTimeout(function(){
                $(".error").hide();
            },5000);
        };
        var nameF = $("#first-name").val();
        var nameL = $("#last-name").val();
        var daTe = $("#start-date").val();
        var addRess = $("#address").val();
        var City = $("#city").val();
        var sta = $("#state").val();
        var zipCode=$("#zip").val();
        var phoneNum = $("#num").val();
        var mail = $("#mail-ad").val();
        var password1 =$("#pass1").val();
        var password2=$("#pass2").val();
        //to check if any of the fields is empty
        if (nameF == ''|| nameL=='' || daTe=='' || addRess=='' ||City === '' ||sta === '' || zipCode=='' || phoneNum== '' || mail=='' || password1=='' || password2=='')
        {
             $("#err6").show();
             showError();
        }
        else 
        {   //to check the name fields
            if(!nameF.match(/^[A-Za-z]+$/))
             { 
                 $("#err1").show();
                 showError();
             }
             if(!nameL.match(/^[A-Za-z]+$/))
             { 
                 $("#err1").show();
                 showError();
             }
            //to check if the phone number is in the correct format
            var len = phoneNum.length;
            var check1= phoneNum.charAt(3);
            var check2 = phoneNum.charAt(7);
            if (check1!="-" || check2!="-" || len!=12)
             {   //var formatted= phoneNum.substr(0,3)+"-"+phoneNum.substr(3,3)+"-"+phoneNum.substr(6,4);
                 //$("#num").attr("value",formatted);
                 //console.log(formatted);
                 $("#err3").show();
                 showError();
             }
            //to check if the passwords entered are identical
            var p1=password1.length;
            var p2=password2.length;
            if (!password1.match(password2) || p1!=p2)
             {
                $("#err5").show();
                showError();
             }
         }  //end of else 
         alert("Form successfully filled");
    }); //end of submit function
});   //end of ready function



            