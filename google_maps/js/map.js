$("document").ready(function () {
    //default location
    var lat = 30.733148,
            lng = 76.7794179,
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    },
    map = new google.maps.Map(document.getElementById('googleMap'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
    var input = document.getElementById('searchbox');
    var autocomplete = new google.maps.places.Autocomplete(input, {
        types: ["geocode"]
    });

    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();
//autocomplete function
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        infowindow.close();
        var place = autocomplete.getPlace();
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        moveMarker(place.name, place.geometry.location);
    });
//to move the map to the entered location
    $("input").focusin(function () {
        $(document).keypress(function (e) {
            if (e.which == 13) {
                infowindow.close();
                var firstResult = $(".pac-container .pac-item:first").text();

                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({"address": firstResult}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat(),
                                lng = results[0].geometry.location.lng(),
                                placeName = results[0].address_components[0].long_name,
                                latlng = new google.maps.LatLng(lat, lng);

                        moveMarker(placeName, latlng);
                        $("input").val(firstResult);
                    }
                });
            }
        });
    });
    google.maps.event.addListener(map, 'click', function (event) {
        placeMarker(event.latLng);
    });
    //to display latitude and longitude   
    function infoWin(loc)
    {
        $("#submit").click(function ()
        {
            var infowindow1;
            infowindow1 = new google.maps.InfoWindow({
                content: 'Latitude: ' + loc.lat() + '<br>Longitude: ' + loc.lng()});
            infowindow1.open(map, marker);
            setTimeout(function () {
                infowindow1.close();
            }, 3000);
        });

    }
// to move the marker where the user clicks the map and display the address
    function placeMarker(location) {

        marker.setPosition(location);
        marker.setMap(map);
        infoWin(location);
        var geocoder;
        geocoder = new google.maps.Geocoder();
        var lat = location.lat();
        var lng = location.lng();
        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({'latLng': latlng}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                results[1] = results[1].formatted_address;
                $("#searchbox").attr("value", results[1]);

            }
            else {
                alert('No results found');
            }
        });
    }
//to move the marker to the input location     
    function moveMarker(placeName, latlng) {
        marker.setIcon(image);
        marker.setPosition(latlng);
        infowindow.setContent(placeName);
        infowindow.open(map, marker);
        setTimeout(function () {
            infowindow.close();
        }, 2000);
        infoWin(latlng);

    }
});